## What is a Jupyter Notebook?

In 2001, IPython was developed as a command environment for the Python programming language. Essentially a fancy terminal for analyzing data with Python tools. In 2014, the good people at the IPython project developed a GUI (Graphic User Interface) for their terminal, and called it the **Jupyter Notebook**. 

The name "Jupyter" comes from the programming languages that are primarily supported: Julia, Python, and R. Each Jupyter Notebook has a "**kernel**" or a core environment that runs the code you put in the Notebook. For example, when you open up a Jupyter Notebook with a Python kernel, any script written within must be written in Python. To get to the Jupyter interface, we launch a **Jupyter server**.

You now have Docker installed, and you should have a idea of how Docker works. To run a basic container that will start a Jupyter server, first copy and paste this line into your terminal and run it.

    docker run -p 8888:8888 registry.gitlab.com/lknowles/unavcodrafts:latest

Running this will provide a URL for you to paste into your preferred web browser. This URL is of the form `http://<hostname>:8888/?token=<token>`, where the `<hostname>` is the name of the computer running the Jupyter server, and the `<token>` is the secret token in the output. `8888` is the host port on which the Jupyter server will run. The URL will appear in the last line of the output and will look like (with letters/numbers instead of X's): `http://(XXXXXXXXXXXX or XXX.X.X.X):8888/?token=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX`

Go ahead and paste the URL from the terminal output with one of the two given hostnames into your web browser. This will bring up the **Jupyter dashboard**! This is our home page when working with Jupyter notebooks. We are currently in the home directory.

**Click on the file "jupytertutorial.ipynb."** It will take you to a Jupyter notebook, where you will get some experience, before directing you back to this introduction.

## Jupyter and Docker

Welcome back to your introduction to Docker and Jupyter! Docker is perfect for running Jupyter notebooks. Jupyter Project has developed several Docker images that contain Jupyter and several other useful data analysis tools. These images are called the **Jupyter Docker Stacks**. The following are a few examples that have been provided by Jupyter Project:

**Example 1:** This command pulls the `jupyter/scipy-notebook` image tagged `17aba6048f44` from Docker Hub if it is not already present on the local host. The tag is simply the name of the version you'll be pulling. If you leave the tag off, Docker will run the latest version from the Hub. The command below will start a container on host port 8888, and run a Jupyter Notebook server on the container port 8888. The container remains intact for restart after the notebook server exits.

    docker run -p 8888:8888 jupyter/scipy-notebook:17aba6048f44

**When the dashboard is open, try starting a new notebook.** 

1.  You are currently in the container's home directory. Go into the directory `work`.
2.  Click on `New` and click `Python 3` to start a notebook with a Python kernel. 
3.  Rename the notebook by clicking on `Untitled` next to the Jupyter logo at the top left and call it "My New Notebook." 
4.  Click into the empty cell. Type `import numpy` and hit `shift-enter`. 

Unlike when we did this with the Jupyter Tutorial notebook, running the cell won't give us an error. That's because the "scipy-notebook" Docker container has a number of useful Python packages installed. If you want to see a comprehesive list of all of the applications and Python packages that are included in this container, type `help("modules")` into the next cell and execute it.

5.  Now go to the `File` tab and hit `Save and Checkpoint`, then click `Close and Halt`. 
6.  Quit the Jupyter server. 
7.  Now run the **Example 1** command again and paste the URL into your browser. 

When you reach the Jupyter dashboard again, go into the `work` folder and you will find that your "My New Notebook.ipynb" is gone. This is the nature of containers: they hold the environment you need to do your work, but do not store your work as they would if you were running these tasks on your local host. Further on we will give you the tools you need to make sure your work is permanent. 

**Example 2:** This command performs the same operations as **Example 1**, but it runs (or "exposes") the container on the host port 10000, and starts a Jupyter notebook on the container port 8888. Being able to specify the host port is useful if we want to run multiple Docker containers. **Important!** This time, instead of copying and pasting the output URL into your browser, you will need to change it to have the form `http://<hostname>:10000/?token=<token>`, where 8888 is replaced with 10000. 

    docker run -p 10000:8888 jupyter/scipy-notebook:17aba6048f44

There are oh, so many ways to run a container with Docker. **Check out the [Docker Documentation](https://docs.docker.com/v17.12/edge/engine/reference/run/) on the `run` command** to find out all of the ways you can customize the way your Container runs. 

## Jupyter Lab

Jupyter Lab is the next generation of the Jupyter Notebook. Jupyter Lab has all of the same tools as the Jupyter Notebook, but with a richer and more flexible user interface. Before we get started, **let's create a folder on our host machine that we can put our data analysis stuff into**. 

1.  Shut down any open Jupyter servers. 
2.  From the terminal, navigate to your home directory by typing `cd ~`. 
3.  Make the directory where we can save our stuff. Type `mkdir jupyterdata`.
4.  Navigate into that directory by typing `cd jupyterdata`.

**Now you will run the following command**. The `--rm` is a "flag," which is something that tells the core command (in this case, `docker run`) to alter its default actions. This flag means that Docker will start an *ephemeral* container, meaning Docker will destroy the container after notebook server exits. Like **Example 2**, it will run a Jupyter server and expose the server on host port 10000. Again, enter the URL into your browser as `http://<hostname>:10000/?token=<token>`.

    docker run --rm -p 10000:8888 -e JUPYTER_ENABLE_LAB=yes -v "$PWD":/home/jovyan/work jupyter/datascience-notebook:9b06df75e445

Welcome to Jupyter Lab! You'll find all of the utilities you've come to expect from the Jupyter Notebook, plus more! At the top is the familiar menu bar. To the left is a new sidebar. The folder icon at the top of the sidebar lets you navigate through your files. The running person icon below it gives you a list of running tasks and the opportunity to shut them down. At the center of everything is the **Launcher**, where you can start a Notebook, Console, terminal, or text file. 

Jupyter Lab has so many capabilities, and you'll want to learn them! Check out this [well-written introduction to Jupyter Labs](https://towardsdatascience.com/jupyter-lab-evolution-of-the-jupyter-notebook-5297cacde6b) to be up to speed on everything Jupyter Labs has to offer!

**Let's dive in!** 

1.  Upon starting, the Jupyter Lab put you into the home directory on the container. You should see that there is only one file called `work`. Nothing new here. 
2.  Navigate into the `work` folder.
3.  Start a Jupyter Notebook.
4.  Go to `File` and click `Rename Notebook...`. Rename the Notebook to `My Newer Notebook.ipynb`.
4.  In the blank cell type `print("I'm still here!")`. Hit `shift-enter`. 
5.  Save the notebook, then shut it down. Terminate the Jupyter server (and the Container) by going to `File` in the menu bar and clicking `Quit` at the very bottom of the dropdown list.

Now that we've closed the container, **let's execute the above `docker run` command again!**

6.  Follow the URL to the Jupyter Lab (don't forget to change `8888` to `10000`)
7.  Navigate into the work folder. 
8.  Rejoice! The "My Newer Notebook.ipynb" is still there! Open it to confirm that your stuff was saved.

What was different? This time, we used a "**volume**." In DockerSpeak, **a volume is a directory on the host machine that we link to the directories in the Docker container**. The `docker run` command included this: `-v "$PWD":/home/jovyan/work`. This part of the command told Docker to link the container's `work` directory to the directory we were currently inside on the host (the PWD, or the print working directory). If we had written `-v ~/jupyterdata:/home/jovyan/work` instead, the command would have worked exactly the same. 

Thus, we could save any files written to `work` on the container because they were also being written to our host directory `jupyterdata`. There are plenty of ways to link the container to the host machine in order to save any work you do within the container. Check out the Docker docs for more info on volumes: https://docs.docker.com/storage/volumes/




