FROM jupyter/base-notebook

MAINTAINER lisaknowles "ljose@email.arizona.edu"

USER root

RUN wget https://gitlab.com/lknowles/unavco-jupyter-nbs/raw/master/jupytertutorial.ipynb

USER jovyan
